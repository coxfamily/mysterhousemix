#!/bin/bash
set -eux
mkdir -p log
qsynth 1>> log/mysterhousemix-qsynth.log 2>>log/mysterhousemix-qsynth.err &
zynaddsubfx \
  -I alsa \
  -O jack \
  -l mysteryhousemix.xmz 1>> log/mysteryhousemix.log 2>>log/mysteryhousemix.err &
sleep 2
qtractor mysteryhousemix.qtr 1>> log/mysterhousemix-qtractor.log 2>>log/mysterhousemix-qtractor.err &
