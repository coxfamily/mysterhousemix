import React from "react"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Img from "gatsby-image"
import MYSTERYHOUSEMIXMP4 from "../assets/mysteryhousemix-export-4.mp4"

const MysterHouseMixPage = props => (
  <Layout>
    <SEO title="MysterHouseMix" />
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
      <h2>December the 24th, 2019</h2>
      <p>
        <audio controls >
          <source src={MYSTERYHOUSEMIXMP4} type="audio/mp4" />
        </audio>
      </p>
  </Layout>
)

export default MysterHouseMixPage

export const fluidImage = graphql`
  fragment fluidImage on File {
    childImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid
      }
    }
  }
`;
